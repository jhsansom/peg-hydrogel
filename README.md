# PEG Hydrogel Experimental Package
Authors: Jake Sansom and Emma Marie Lejeune


## Introduction
This software was used as the computational backend for Alex Khang's experiments with animal-source VIC seeded PEG hydrogels.

## Methods

## Installation
Using Anaconda, peg-hydrogel is easy to install and get up and running. The first step would be to download the repository by entering this command in terminal:

```
git clone https://jhsansom@bitbucket.org/jhsansom/peg-hydrogel.git
```

Next, you must create a conda environment and install the correct packages into it. This can be done using these commands:

```
conda create -n fenicsproject -c conda-forge python=3.6 fenics mshr ipython
source activate fenicsproject
conda install pandas numpy matplotlib scipy xlrd
```

This conda environment can be toggled on and off using the commands `source activate fenicsproject` and `source deactivate` respectively. 

## Implementation
The only files you will need to change to use the experimental package are the data.xlsx file and experiment.py. To properly input your data, you can do it one of two ways.
Either way, you must specify the name of the data file near the top of experiment.py and save it in the same directory as the software.

1. You can save the raw data as a .xlsx file, formatted exactly like the example file is. Some columns may need to be deleted to ensure the software intakes the correct data points.
2. You can save the data as a modified .xlsx file, with moment values in column A and displacement values in column B. Label each column with headers, or the software will not intake the first row. If you do this, change is_raw_data at the top of experiment.py to False.

After doing this, you must enter in the length, width, and height (thickness) of the hydrogel into experiment.py, along with the initial curvature value.

Once you are done following these two steps, you can run grad.py in the fenicsproject environment using the command `python grad.py`.


## Other Features
If you would like to access the raw data of any specific iteration, you can do so using the following commands. You can easily view the parameters of the experiment class in experiment.py.

```text
from experiment import *
exp = open_experiment('<path-to-data.pkl-file>')

do-something-with(exp.some-parameter)
```

If you would like to remove the data created by an old test and restart the test with new values, you can run the cleanup.sh script. This will remove all iteration, jit failure, and \__pycache__ files.

## Frequently Encountered Problems
If your terminal tells you that there is no module named dolfin, run the following commands to remove your current environment and update conda itself. Then, re-create your fenicsproject environemnt using the instructions above.

```text
conda remove env -n fenicsproject
conda update conda
```

If your terminal tells you there is no module named _common, run `conda update --all` within your environment. This should fix it.
