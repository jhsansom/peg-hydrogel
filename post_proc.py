## This file contains all of the post processing functions for an individual experimental trial.
## By importing sim as a module, it also runs the physical experiment.

# Other import statements
from scipy import stats, interpolate
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import pandas as pd
from experiment import *
import sim

def run_post_proc (num, run_sim):
    # Runs sim module
    if run_sim:
        sim.run_sim(num, True)

    # Imports experimental data
    exp = open_experiment('./iteration%i/data.pkl'%num)

    ideal_disp = exp.ideal_disp
    exp_data = exp.exp_data
    
    i = 0
    while i < len(exp_data[0,:]):
        if exp_data[0,i] > 3:
            break
        i += 1

    exp_data = exp_data[:,0:i-1]

    sim_force = exp.force_array
    sim_disp = exp.u_array

    x_lo = -exp.length/2; x_hi = exp.length/2; y_lo = -exp.height/2; y_hi = exp.height/2; z_lo = -exp.width/2; z_hi = exp.width/2

    def get_mi(P,y):
        b = (z_hi - z_lo)
        h = (y_hi - y_lo)
        I = b * h ** 3.0 / 12.0
        M = P * y
        M_over_I = M / I
        return abs(M_over_I)
        
    m_list = []

    num_step = sim_force.shape[0]
    
    
    plt.figure()
    for kk in range(0,num_step):
        P = sim_force[kk,0]
        y = sim_disp[kk,7]
        m_list.append(P*y)
    
    plt.axis('equal')
    plt.savefig('./iteration%i/curves'%(num))

    plt.figure()
    applied_disp_list = exp.disp_list - ideal_disp*np.ones((len(exp.disp_list)))
    applied_disp  = [] 
    for kk in range(0,len(applied_disp_list)):
        applied_disp.append(-1*applied_disp_list[kk])

    i = 0
    while i < len(applied_disp):
        if applied_disp[i] > 3:
            break
        i += 1

    applied_disp = applied_disp[0:i-1]
    m_list = m_list[0:i-1]

    exp.sim_data = np.vstack((applied_disp, m_list))

    # Finds residuals and error
    sim_func = interpolate.interp1d(applied_disp, m_list, fill_value='extrapolate')
    residuals = np.array([])
    for i in range(len(exp_data[0,:])):
        residuals = np.append(residuals, exp_data[1,i] - sim_func(exp_data[0,i]))
    exp.error = np.dot(residuals, residuals)
    exp.residuals = residuals

    # Finds R^2 value
    avg = sum(exp_data[1,:]) / len(exp_data[1,:])
    norm_array = exp_data[1,:] - avg
    sstot = np.dot(norm_array, norm_array)
    exp.rsquared = 1 - (exp.error / sstot)
    
    save_experiment('./iteration%i/data.pkl'%num, exp)

    # Graphs moment displacement curve
    plt.scatter(exp_data[0,:],exp_data[1,:], c='r', s=10)
    plt.plot(applied_disp, sim_func(applied_disp), 'k')
    plt.ylabel('Moment (mN*mm)')
    plt.xlabel('Displacement (mm)')
    plt.legend(['Neo-Hookean Model Fit', 'Experimental Data'])
    plt.text(0.05, 0.95, '$\mu=$%.5f \n $R^2=$%.3f'%(exp.mu, exp.rsquared))
    plt.savefig('./iteration%i/graph_disp'%(num))

    # Graphs residuals
    plt.clf()
    plt.axhline(y=0, color='k')
    plt.scatter(exp_data[0,:],residuals, c='r', s=10)
    plt.ylabel('Moment Residuals (mN*mm)')
    plt.xlabel('Displacement (mm)')
    plt.legend(['Neo-Hookean Model Fit', 'Experimental Data'])
    plt.text(0.05, 0.05, '$\mu=$%.5f \n $R^2=$%.3f'%(exp.mu, exp.rsquared))
    plt.savefig('./iteration%i/graph_disp_norm'%(num))

